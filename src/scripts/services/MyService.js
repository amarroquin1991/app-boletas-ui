angular.module("myApp").factory("MyService", function ($http) {
    function make_base_auth(user, password) {
        var tok = user + ':' + password;
        var hash = btoa(tok);
        return "Basic " + hash;
    }
    return {
  
    sync_auth: function (view) {
            console.log(view);
            return $http({
                method: 'POST',
                url: 'https://apps-offymarket.000webhostapp.com/api/v3/auth/login',
                data: view,
                headers: {'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                return response;
            });
        },
    sync_home: function (view) {
            console.log(view);
            return $http({
                method: 'POST',
                url: 'https://apps-offymarket.000webhostapp.com/api/v3/get/home/'+view,
                data: {},
                headers: {'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                return response;
            });
        },
    sync_boletas: function (view) {
            console.log(view);
            return $http({
                method: 'POST',
                url: 'https://apps-offymarket.000webhostapp.com/api/v3/get/boletas/'+view,
                data: {},
                headers: {'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                return response;
            });
        },
    sync_detalle: function (view) {
            console.log(view);
            return $http({
                method: 'POST',
                url: 'https://apps-offymarket.000webhostapp.com/api/v3/get/recivo/'+view,
                data: {},
                headers: {'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                return response;
            });
        },    sync_dashs: function () {
    
            return $http({
                method: 'GET',
                url: 'https://apps-offymarket.000webhostapp.com/api/v3/get/dash',
                data: {},
                headers: {'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                return response;
            });
        },
           empresas_sync: function () {
    
            return $http({
                method: 'GET',
                url: 'https://apps-offymarket.000webhostapp.com/api/v3/empresas',
                data: {},
                headers: {'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                return response;
            });
        },
           puestos_sync: function () {
    
            return $http({
                method: 'GET',
                url: 'https://apps-offymarket.000webhostapp.com/api/v3/puestos',
                data: {},
                headers: {'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                return response;
            });
        },
          roles_sync: function () {
    
            return $http({
                method: 'GET',
                url: 'https://apps-offymarket.000webhostapp.com/api/v3/roles',
                data: {},
                headers: {'Content-Type': 'application/json'}
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                return response;
            });
        }
    };

});