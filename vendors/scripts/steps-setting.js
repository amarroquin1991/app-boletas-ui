    $(".tab-wizard").steps({
	headerTag: "h5",
	bodyTag: "section",
	transitionEffect: "fade",
	titleTemplate: '<span class="step">#index#</span> #title#',
	labels: {
		finish: "Submit"
	},
	onStepChanged: function (event, currentIndex, priorIndex) {
		$('.steps .current').prevAll().addClass('disabled');
	},
	onFinished: function (event, currentIndex) {
		$('#success-modal').modal('show');
	}
});

$(".tab-wizard2").steps({
	headerTag: "h5",
	bodyTag: "section",
	transitionEffect: "fade",
	titleTemplate: '<span class="step">#index#</span> <span class="info">#title#</span>',
	labels: {
		finish: "Completar",
		next: "Continuar",
		previous: "Regresar",
	},
	onStepChanged: function(event, currentIndex, priorIndex) {
		$('.steps .current').prevAll().addClass('disabled');
	},
	onFinished: function(event, currentIndex) {
            
            if($('input[name="chk-acepto"]').is(':checked'))
{
 var desc = $("#desc-per").val();
            var observ = $("#observ-per").val();
               var periodo = $("#periodo").val();
                var ciclo = $("#ciclo").val();

            $.ajax({
                url: "https://apps-offymarket.000webhostapp.com/api/v3/new/periodo",
                method: "POST",
                data: {
                    mode: 'POST',
                    periodo: periodo, ciclo: ciclo, desc:desc, obs:observ
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce');
                },
                success: function (response) {
                    console.log(response);
                    if (response.error) {
                        alert(response.message);
                    }else{
                //  $('#success-modal-btn').trigger('click');
                  $('#success-modal').modal('show');
                    }
                    
                },
                error: function (xhr, textStatus, error) {
                    alert("Error: Ocurrio algo durente el proceso de creacion");
                    console.log(xhr);
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                }
            });
}else
{
alert("Es necesario validar la informacion");
}
                
            
		//
	}
});



$(".tab-wizard3").steps({
	headerTag: "h5",
	bodyTag: "section",
	transitionEffect: "fade",
	titleTemplate: '<span class="step">#index#</span> <span class="info">#title#</span>',
	labels: {
		finish: "Completar",
		next: "Continuar",
		previous: "Regresar",
	},
	onStepChanged: function(event, currentIndex, priorIndex) {
		$('.steps .current').prevAll().addClass('disabled');
	},
	onFinished: function(event, currentIndex) {
            
            if($('input[name="chk-acepto"]').is(':checked'))
{
    var email = $("#email").val();
            var name = $("#name").val();
            //alert($("#pasw").val());
               var pasw  = $.md5($("#pasw").val());
                var empresa = $("#empresa").val();
                 var puesto = $("#puesto").val();
var perfil = $("#perfil").val();
            $.ajax({
                url: "https://apps-offymarket.000webhostapp.com/api/v3/new/create",
                method: "POST",
                data: {
                    mode: 'POST',
                    nombre: name, email: email, pasw:pasw, empresa:empresa, rol:perfil, puesto:puesto
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-WP-Nonce');
                },
                success: function (response) {
                    console.log(response);
                    if (response.error) {
                        alert(response.message);
                    }else{
                //  $('#success-modal-btn').trigger('click');
                  $('#success-modal').modal({backdrop: 'static', keyboard: false});
                  
                    }
                    
                },
                error: function (xhr, textStatus, error) {
                    alert("Error: Ocurrio algo durente el proceso de creacion");
                    console.log(xhr);
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                }
            });
}else
{
alert("Es necesario validar la informacion");
}
                
            
		//
	}
});