var app = angular.module('myApp', ['angular-md5']);
app.controller('loginCtrl', function ($scope, MyService, md5, $window) {
    console.log("carga del controlador...");
    $scope.logindata = {};
    $scope.is_login = true;
    $scope.logindata.user = "alejandro.alejandro.marroquin@gmail.com";
    $scope.logindata.psw = "Seguridad2016";

    $scope.initView = function () {
        if (localStorage.getItem("user_id") == null) {
            console.log("sesion empty");
        } else {
            $window.location.href = 'home.html';
        }
    };
    $scope.loginAuth = function () {
        $scope.logindata.psw = md5.createHash($scope.logindata.psw || '');
        console.log("Send to server...");
        MyService.sync_auth($scope.logindata).then(function (data) {
            console.log(data.data);
            if (data.data.error) {
                Swal.fire({
                    title: 'Lo sentimos!',
                    text: data.data.message
                })
            } else {
                console.log(data.data.user.user_id);
                localStorage.setItem('user_id', data.data.user.user_id);
                localStorage.setItem('user_name', data.data.user.name);
                localStorage.setItem('user_empresa_id', data.data.user.empresa_id);
                localStorage.setItem('user_empresa_name', data.data.user.empresa);
                localStorage.setItem('user_created_at', data.data.user.created_at);
                localStorage.setItem('is_admin', data.data.user.is_admin);
                $window.location.href = 'home.html';
            }

        });
    };

    $scope.setIsLoginView = function (is_loging) {
        if (is_loging) {
            $scope.is_login = false;
        } else {
            $scope.is_login = true;
        }
    };

});