var app = angular.module('myApp', []);
app.controller('homeCtrl', function ($scope, MyService,$window) {
console.log("cargando controlador...." +localStorage.getItem("user_name"));
$scope.viewhome=[];
$scope.viewdash={};
$scope.boletas=[];
$scope.render_view=null;
$scope.renderdata={};
$scope.temp_data={};
$scope.temp_total=0.0;
$scope.detail_boleta=[];
$scope.empresas=[];
$scope.puestos=[];
$scope.roles=[];
$scope.temp_data.descp=null;
$scope.temp_data.observ=null;
$scope.temp_data.nom="Carlos Marroquin";
$scope.temp_data.email="carlopez@gmail.com";
    $scope.temp_data.period=null;
$scope.temp_data.ciclo=null;
$scope.renderdata.user_name=localStorage.getItem("user_name");
 $scope.renderdata.is_admin=localStorage.getItem("is_admin");
 $scope.initSesion = function () {
        if (localStorage.getItem("user_id") == null) {
         $window.location.href = 'index.html';
        }
    };
    $scope.initSesion();
    $scope.loadColaboradores=function (){
        MyService.sync_home($scope.renderdata.is_admin=="true"?"admin":"sample").then(function (data) {
            console.log(data.data);
            $scope.viewhome=data.data;
        });
    }
    
    $scope.loadDash=function (){
        MyService.sync_dashs().then(function (data) {
            console.log(data.data);
            $scope.viewdash=data.data;
            
        });
    };
    
     $scope.loadConfigs=function (){
        MyService.empresas_sync().then(function (data) {
            console.log(data.data);
            $scope.empresas=data.data;
            
        });
        
          MyService.puestos_sync().then(function (data) {
            console.log(data.data);
            $scope.puestos=data.data;
            
        });
        
         MyService.roles_sync().then(function (data) {
            console.log(data.data);
            $scope.roles=data.data;
            
        });
    };
    
  
   
    
    
    
     $scope.loadInBoletas = function (id_user) {
     console.log(localStorage.getItem("is_admin"));
  
        console.log("Send to server...");
        MyService.sync_boletas(id_user).then(function (data) {
            console.log(data.data);
            $scope.boletas=data.data;
        });
    };
    
     $scope.initView = function () {
     console.log(localStorage.getItem("is_admin"));
       
        console.log("Send to server...");
        if($scope.renderdata.is_admin=="true"){
            $scope.loadColaboradores();
            $scope.loadDash();
        }else{
                $scope.loadInBoletas(localStorage.getItem("user_id"));
        }
    };
      $scope.calculeTotal=function (){
         $scope.temp_total=0.0;
        angular.forEach($scope.detail_boleta, function(value, key){
            console.log(value.is_deducible+":"+value.valor +",  sub:"+$scope.temp_total);
      if(value.is_deducible == "true"){
           $scope.temp_total=parseInt($scope.temp_total)-parseInt(value.valor);
      }else{
           $scope.temp_total=parseInt($scope.temp_total)+parseInt(value.valor);
      }
      
   });
    };
    $scope.load_detail = function () {
        $scope.temp_total=0.0;
        
     console.log(localStorage.getItem("print_ref"));
     var print_ref=localStorage.getItem("print_ref");
        console.log("Send to server...");
        MyService.sync_detalle(print_ref).then(function (data) {
            console.log(data.data);
            if(data.data.length<=0){
        alert("No se encontraron detalles del recibo, contactate con el departamento tecnico");
        $window.close();
            }else{
                $scope.detail_boleta=data.data;
                     $scope.calculeTotal(); 
            }
            
        });
    };
  
    
    $scope.clearBoletas = function () {
      $scope.boletas=null;
        $scope.boletas=[];
    };
    
    
    
      $scope.view_recivo=function (id_enc){
        localStorage.setItem("print_ref", id_enc);
        console.log("detalles #"+localStorage.getItem("print_ref"));
         $window.open('invoice.html?ref='+id_enc, '_blank');
          //$window.location.href = 'invoice.html?ref='+id_enc;
    }; 
    

    $scope.logout=function (){
        localStorage.clear();
         $window.location.href = 'index.html?ref=out';
    };
});